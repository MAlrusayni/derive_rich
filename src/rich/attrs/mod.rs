pub(crate) mod common;
pub(crate) mod read_fn;
pub(crate) mod value_fns;
pub(crate) mod write_fn;

use syn::{
    parse::{Parse, ParseStream, Result},
    Ident,
};

use self::{read_fn::ReadFn, value_fns::ValueFns, write_fn::WriteFn};

#[derive(Debug)]
pub(crate) enum Attribute {
    ValueFns(ValueFns),
    ReadFn(ReadFn),
    WriteFn(WriteFn),
}

impl Parse for Attribute {
    fn parse(input: ParseStream) -> Result<Self> {
        match input.fork().parse::<Ident>()?.to_string().as_str() {
            "value_fns" => input.parse::<ValueFns>().map(Attribute::ValueFns),
            "read" => input.parse::<ReadFn>().map(Attribute::ReadFn),
            "write" => input.parse::<WriteFn>().map(Attribute::WriteFn),
            _ => Err(input.error("`rich` only accept `value_fns`, `read` and `write` attributes"))?,
        }
    }
}
