use quote::{quote, TokenStreamExt};
use syn::parse::{Parse, ParseStream, Result};

#[derive(Debug)]
pub(crate) enum Visibility {
    Private(syn::Token![priv]),
    Visibility(syn::Visibility),
}

impl Parse for Visibility {
    fn parse(input: ParseStream) -> Result<Self> {
        let lookahead = input.lookahead1();
        if lookahead.peek(syn::Token![priv]) {
            input.parse().map(Visibility::Private)
        } else {
            input.parse().map(Visibility::Visibility)
        }
    }
}

impl quote::ToTokens for Visibility {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        match self {
            Self::Private(_) => {}
            Self::Visibility(vis) => tokens.append_all(quote! { #vis }),
        }
    }
}
