pub(crate) mod rich;

extern crate proc_macro;

use crate::rich::expand_derive_rich;
use proc_macro::TokenStream;

#[proc_macro_derive(Rich, attributes(rich))]
pub fn derive_model(input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as syn::DeriveInput);
    expand_derive_rich(&input)
        .unwrap_or_else(|err| err.to_compile_error())
        .into()
}
